/*
 * AT commands for configuring a slave HC05
 * 
 * Tie the EN pin on the HC05 to VCC. Open the Serial Monitor, make sure the 
 * line endings are set to 'Both NL and CR'. If your HC05 is set up correctly,
 * the LED should be flashing a lot slower, and typing AT into the prompt 
 * should return OK.
 * 
 * AT+UART? should return +UART:38400,0,0
 * AT+ROLE? should return +ROLE:0
 * AT+ADDR? should return the MAC address of the HC05
 * 
 * If AT+UART? or AT+ROLE? do not return the expected values, set them with
 * AT+UART=38400,0,0
 * AT+ROLE=0
 */

#include <SoftwareSerial.h>

const long baud = 38400L;

const int rx = 5;
const int tx = 6;

void setup()
{
    Serial.begin(baud);
    bluetooth.begin(baud);
}

void loop()
{
    if (bluetooth.available())
        Serial.write(bluetooth.read());

    if (Serial.available())
        bluetooth.write(Serial.read());
}